Dutch Prime Minister Mark Rutte's party has won the most seats in parliamentary elections, exit polls say.
Early results confirmed the exit polls, with his centre-right VVD Party on course for 31 out of 150 seats.
Three parties are projected to win 19 seats each: Geert Wilders' anti-immigration Freedom Party (PVV), the Christian Democrats and the D66 party.
Mr Wilders' party had been leading in opinion polls but support for the party appeared to slip in recent days.
With 10.9% of votes counted early on Thursday, the VVD had polled 17.8%.
Voter participation in the general election was high; the 81% turnout was the highest for 30 years.
Analysts say a high turnout may have benefited pro-EU and liberal parties.
"Today was a celebration of democracy," Mr Rutte said, adding that the Netherlands had said no to the "wrong kind of populism".
Although the VVD had lost several seats since the last election, many had expected the party to lose much more ground to the Freedom Party.
Members of GroenLinks during election night in Amsterdam, The Netherlands, 15 March 2017Image copyrightEPA
Image caption
Also celebrating were the Green Left party, that gained several seats
Many had been watching the vote in the Netherlands closely, as an indication for how populist parties may fare in other elections in EU countries.
France goes to the polls next month to elect a new president, while Germany is due to hold a general election in September.
German Chancellor Angela Merkel has called Mr Rutte to congratulate him, while Luxembourg Prime Minister Xavier Bettel has also tweeted his congratulations.
More on this story:
Voting Dutch: Quirky polling stations
Dutch vote turns spotlight on Moroccans
Political pets - where the virtual life of a politician is in your hands
Martin Schulz, president of the European Parliament until earlier this year, said he was "relieved" Mr Wilders' party had lost.
"We must continue to fight for an open and free Europe!" he added on Twitter (in German).
However, Mr Wilders warned that Mr Rutte "has not seen the last of me".
He previously said that the "patriotic revolution" would continue to take place, and "the genie will not go back into the bottle".
